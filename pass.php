<?php

/*$rsaKey = openssl_pkey_new(array( 
    'private_key_bits' => 4096,
    'private_key_type' => OPENSSL_KEYTYPE_RSA,
));
$privKey = openssl_pkey_get_private($rsaKey); 
openssl_pkey_export($privKey, $pem);

echo $pem;
*/

function isCLI(){
    if(defined('STDIN')){
        return true;
    }

    if(php_sapi_name() === 'cli'){
        return true;
    }

    if(array_key_exists('SHELL', $_ENV)){
        return true;
    }


    return false;
}
function getRandomPassword(
	$length,
	$allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@$#/{}[]()+&" 
	){

	if($length === ""){
		$length = random_int(20, 50);
	}
	$allowedCharsLength = strlen($allowedChars);
	$newPassword = "";

	for($i=0; $i<$length; $i++){
		$index = random_int(0, $allowedCharsLength-1);	//CRYPTOGRAPHICALLY SECURE
		$newPassword .= $allowedChars[$index];
	}

	return $newPassword;
}

if(isCLI()){
	echo "To get random length between 20 and 50 just press Enter" . PHP_EOL;
	$neededLength = readline("Length of Password: ");
	if($neededLength === "" || (is_numeric($neededLength) && intval($neededLength)>-1)){
		echo getRandomPassword($neededLength);
	}else{
		echo "Invalid Length";
	}
}else{
    echo getRandomPassword("");
}

echo PHP_EOL;
?>
